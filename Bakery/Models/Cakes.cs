﻿using System;
using System.Collections.Generic;

namespace Bakery.Models
{
    public partial class Cakes
    {
        public Cakes()
        {
            Orders = new HashSet<Orders>();
        }

        public int IdCake { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }

        public ICollection<Orders> Orders { get; set; }
    }
}
