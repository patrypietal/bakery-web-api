﻿using System;
using System.Collections.Generic;

namespace Bakery.Models
{
    public partial class Customers
    {
        public Customers()
        {
            Orders = new HashSet<Orders>();
        }

        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Nip { get; set; }

        public ICollection<Orders> Orders { get; set; }
    }
}
