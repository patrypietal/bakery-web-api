﻿using System;
using System.Collections.Generic;

namespace Bakery.Models
{
    public partial class Orders
    {
        public int IdOrder { get; set; }
        public int? IdClient { get; set; }
        public int IdCake { get; set; }
        public double Quantity { get; set; }
        public decimal AmountPrice { get; set; }
        public DateTime Date { get; set; }

        public Cakes IdCakeNavigation { get; set; }
        public Customers IdClientNavigation { get; set; }
    }
}
