﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Bakery.Models;

namespace Bakery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CakesController : ControllerBase
    {
        private readonly bakery1Context _context;

        public CakesController(bakery1Context context)
        {
            _context = context;
        }


        /// <summary>
        /// Returns all cake items
        /// </summary>
        [HttpGet]
        public IEnumerable<Cakes> GetCakes()
        {
            return _context.Cakes;
        }


        /// <summary>
        /// Returns id specified cake item
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCakes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cakes = await _context.Cakes.FindAsync(id);

            if (cakes == null)
            {
                return NotFound();
            }

            return Ok(cakes);
        }

        /// <summary>
        /// Update or replace id specified cake item
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCakes([FromRoute] int id, [FromBody] Cakes cakes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cakes.IdCake)
            {
                return BadRequest();
            }

            _context.Entry(cakes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CakesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create new cake item
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostCakes([FromBody] Cakes cakes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Cakes.Add(cakes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCakes", new { id = cakes.IdCake }, cakes);
        }


        /// <summary>
        /// Delete cake item specified by id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCakes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cakes = await _context.Cakes.FindAsync(id);
            if (cakes == null)
            {
                return NotFound();
            }

            _context.Cakes.Remove(cakes);
            await _context.SaveChangesAsync();

            return Ok(cakes);
        }

        private bool CakesExists(int id)
        {
            return _context.Cakes.Any(e => e.IdCake == id);
        }
    }
}