﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bakery.Models; 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bakery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BakeryController : ControllerBase
    {
        private readonly bakery1Context _context;

        public BakeryController(bakery1Context context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns the orders placed by anonymous clients
        /// </summary>
        [HttpGet("amnonymousOrders")]
        public IEnumerable<Orders> GetAnonymous()
        {
            return _context.Orders.Where(f => f.IdClient == null);
        }
        /// <summary>
        /// Returns the purchase amount of each company
        /// </summary>
        [HttpGet("firmSummary")]
        public JsonResult GetFirmSummary()
        {
            return new JsonResult(_context.Orders
.Join(_context.Customers, o => o.IdClient, c => c.IdCustomer, (c, o) => new { c.IdClient, o.Nip, c.AmountPrice })
.Where(f => f.Nip != null)
.GroupBy(f => f.IdClient)
.Select(n => new { ClientId = n.Key, Amount = n.Sum(f => f.AmountPrice) })
);
        }
        /// <summary>
        /// Returns the bakery's monthly income
        /// </summary>
        [HttpGet("monthsSummary")]
        public JsonResult GetMonthsSummary()
        {
            return new JsonResult(_context.Orders
                .Select(f => new { f.Date.Year, f.Date.Month, f.AmountPrice })
                .GroupBy(x => new { x.Year, x.Month }, (key, group) => new { year = key.Year, key.Month, totalAmount = group.Sum(f => f.AmountPrice) }));
        }


        /// <summary>
        /// Returns the avalible cakes
        /// </summary>
        [HttpGet("avalibleCakes")]
        public IEnumerable<Orders> GetAvalibleCakes()
        {
            return (_context.Orders 
                .Where(f=>f.Quantity>0));
        }


    }
}